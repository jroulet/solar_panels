This repository is aimed to the characterization of the electric response of the solar panels of SPIDER and SuperBIT.

See Section 3 of the report for a description of the methods and results.

Contents:

 plot_solar.ipynb                # Generates the plots in /plots from the data in /data.
                                 # Also estimates the panel equivalent circuit parameters.
 
 /data/index.dat                 # A text file indicating what the other data files are.
 
 /data/<number>.txt              # Data files acquired with my I-V measuring circuit and 
                                 # Steve Benton's code solar_panel_data.py. 
                                 # Files then renamed according to the index file. 
                                 
 /data/labjack_noise.txt         # A data file acquired with the LabJack's inputs disconnected.
