#!/usr/bin/env /usr/bin/python

# get solar panel I-V curve data fram labjack
# Hardware configuration:
#   array voltage (divided) connected to AIN0
#   current sense connected to AIN1
#   SGND used as shared ground

import os
import sys
import time

import u6

d = u6.U6
d = u6.U6()
d.configU6()

outdir = "data"
outname = os.path.join(outdir, "{}.txt".format(time.strftime("%Y-%m-%d_%H:%M:%S")))
f = open(outname, "w")
f.write("Time Voltage Current\n")

print("Acquiring data file: {}".format(outname))

dirname, basename = os.path.split(outname)
linkname = os.path.join(dirname, "current.txt")
if os.path.islink(linkname):
    os.remove(linkname)
os.symlink(basename, linkname)

while True:
    v = d.getAIN(0)
    i = d.getAIN(1)
    f.write("{t} {v} {i}\n".format(t=time.time(), v=v, i=i))
    f.flush()
    time.sleep(0.01)
